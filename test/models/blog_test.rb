# frozen_string_literal: true

# == Schema Information
#
# Table name: blogs
#
#  id             :integer          not null, primary key
#  article        :text             not null
#  published_date :date
#  title          :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#
# Indexes
#
#  index_blogs_on_user_id  (user_id)
#
require 'test_helper'

class BlogTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
